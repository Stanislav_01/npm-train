var changeWordInTExtTest = require("./index.js");
var removeWordTestText = "The English Wikipedia";
test("remove word", function () {
    expect(changeWordInTExtTest(removeWordTestText, "English")).toBe("The Wikipedia");
});
test("change word", function () {
    expect(changeWordInTExtTest(removeWordTestText, "English", "Stas")).toBe("The Stas Wikipedia");
});
//# sourceMappingURL=index.test.js.map