var changeWordInTExt = function (text, word, newWord) {
    if (newWord === void 0) { newWord = null; }
    var result;
    if (!newWord) {
        newWord = "";
    }
    var regExp = new RegExp("" + word, "gmi");
    var newText = text.replace(regExp, "" + newWord);
    result = newText;
    if (!newWord) {
        var regExp2 = new RegExp("  ", "gmi");
        var newText2 = newText.replace(regExp2, " ");
        result = newText2;
    }
    console.log(result);
    return result;
};
module.exports = changeWordInTExt;
//# sourceMappingURL=index.js.map