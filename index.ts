const changeWordInTExt = (text, word, newWord = null) => {
  let result;
  if (!newWord) {
    newWord = "";
  }
  let regExp = new RegExp(`${word}`, "gmi");
  let newText = text.replace(regExp, `${newWord}`);
  result = newText;

  if (!newWord) {
    let regExp2 = new RegExp(`  `, "gmi");
    let newText2 = newText.replace(regExp2, ` `);
    result = newText2;
  }

  console.log(result);
  return result;
};

module.exports=changeWordInTExt;