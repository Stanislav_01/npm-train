const changeWordInTExtTest = require("./index.js");


let removeWordTestText = "The English Wikipedia";

test("remove word", () => {
  expect(changeWordInTExtTest(removeWordTestText, "English")).toBe("The Wikipedia");
});

test("change word", () => {
  expect(changeWordInTExtTest(removeWordTestText, "English", "Stas")).toBe(
    "The Stas Wikipedia"
  );
});
